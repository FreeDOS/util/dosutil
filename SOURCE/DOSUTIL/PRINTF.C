#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define INC_FLOAT

int acnt = 1;
int loop = 0;

char helptxt1[]  = "\n\n\
 PRINTF 1.0 by J�rgen Hoffmann (2010) j_hoff@hrz1.hrz.tu-darmstadt.de\n\n\
 usage:    printf   <format> [ <arg1> <arg2> ... <argn> ]\n\
    or:    printf [ <format> [ <arg1> ... ] ] <  <file>\n\n\
 format:   the following standard \"C\" format specifyers are supported by this\n";
char helptxt2[]  = "\
	   pointer  formats '%%n' and '%%p' are NOT supported\n\
	   variable  width  '*'  is  NOT  supported either\n\n\
 codes:    '\\a', '\\b', '\\f', '\\n', '\\r', '\\t', '\\v', '\\xhex' , '\\0oct'\n\
	   \\e escape character\n\
	   \\q quote  character\n\
	   \\l (loop) begin of format repetition\n\
	   \\m (more) end   of format repetition, begin of trailer\n\n\
 examples: printf \"%%s %%d %%02X %%d %%02X\\n\" abc 123 123 0x7b 0X7B\n\
	   echo abc def ... xyz | printf \"result=\\q\\l%%s,\\m\\q\\n\"\n\n\
 note:     inside batch files literal '%%' signs\n\
	   must be duplicated like  e.g.  \"%%%%2d\"\n\n";

char* next_arg(int argc, char *argv[]) {
  char buf[128], *p, *q;
  int c;
  if(acnt < argc) {
    p = argv[acnt++];
    q = &p[strlen(p)-1];
    if((*p!='%')||(*q!='%')) return(p);
    strcpy(buf,&p[1]);
    q  = &buf[strlen(buf)-1];
    *q = '\0';
    q  = getenv(strupr(buf));
    if(!q) return(p);
    strcpy(buf,q);
    return(buf);
    }
  if(!(stdin->flags&_F_TERM)) {
    buf[0] = '\0';
    p =  buf;
    q = &buf[127];
    c = getc(stdin);
    while((c!=EOF)&&(c<=' ')) c = getc(stdin);
    if(c==EOF) return(NULL);
    if(c!='"') {
      while((c!=EOF)&&(c>' ')) {
	if(p<q) *p++ = c;
	c = getc(stdin);
	}
      }
    else {
      c = getc(stdin);
      if(c==EOF) return(NULL);
      while((c!=EOF)&&(c!='"')) {
	if(p<q) *p++ = c;
	c = getc(stdin);
	}
      }
    *p = '\0';
    acnt++;
    return(buf);
    }
  return(NULL);
  }

void usage(void) {
  printf(helptxt1);
#ifdef INC_FLOAT
  printf("           version: '%%d','%%i','%%o','%%u','%%x','%%X','%%e','%%f','%%g','%%E','%%G'\n");
#else
  printf("           version: '%%d', '%%i', '%%o', '%%u', '%%x', '%%X'\n");
#endif
  printf(helptxt2);
  exit(1);
  }

void main(int argc, char *argv[]) {
  long   iarg;
#ifdef INC_FLOAT
  double rarg;
#endif
  char  *sarg;
  char *f, *f0, *f1, format[128], *t, temp[32];
  if((f = next_arg(argc,argv))==NULL) usage();
  else strcpy(format,f);
  f0 =  format;
  f1 = &format[strlen(format)-1];
  f  =  format;
  sarg = next_arg(argc,argv);
  while(*f) {
    if(*f=='\\') switch(toupper(*(++f))) {
      case 'A':  putc('\a',  stdout);  break;
      case 'B':  putc('\b',  stdout);  break;
      case 'E':  putc('\033',stdout);  break;
      case 'F':  putc('\f',  stdout);  break;
      case 'N':  putc('\n',  stdout);  break;
      case 'R':  putc('\r',  stdout);  break;
      case 'T':  putc('\t',  stdout);  break;
      case 'V':  putc('\v',  stdout);  break;
      case 'Q':  putc('"',   stdout);  break;
      case 'X':  if(isxdigit(f[1])&&isxdigit(f[2])) {
		   temp[0] = f[1];
		   temp[1] = f[2];
		   temp[2] = '\0';
		   putc(strtoul(temp,NULL,16)&0XFF,stdout);
		   f += 2;
		   }                   break;
      case '0':  t = temp;
		 while((*f>='0')&&(*f<='7')) *t++ = *f++;
		 *t = '\0';
		 putc(strtoul(temp,NULL,8)&0XFF,stdout);
		 f--;                  break;
      case 'L':  f0 = &f[1]; loop = 1; break;
      case 'M':  if(loop) { f1 = f;
			     f = f0;
			     f--;    } break;
      case '\\': putc('\\',  stdout);  break;
      }
    else if(*f=='%') {
      f++;
      if(*f=='%') putc('%',stdout);
      else {
	t = temp;
	*t++ = '%';
	if(strchr("-+ #",*f)) *t++ = *f++;
	while(isdigit(*f)||(*f=='.')) *t++ = *f++;
	if(strchr("hlLFN",*f)) f++;
	*t = *f;
	t[1] = '\0';
	if(sarg==NULL) {
	  if(!loop) printf("(NULL)");
	  else { f = f1; loop = 0; }
	  }
	else {
	  if(strchr("diouxX",*t)) {
	    t[2] = '\0';
	    t[1] = *t;
	    *t = 'l';
	    iarg = strtol(sarg,NULL,0);
	    printf(temp,iarg);
	    }
#ifdef INC_FLOAT
	  else if(strchr("efgEG",*t)) {
	    t[2] = '\0';
	    t[1] = *t;
	    *t = 'l';
	    rarg = strtod(sarg,NULL);
	    printf(temp,rarg);
	    }
#endif
	  else if(*t=='s') printf(temp,sarg);
	  else if(*t=='c') {
	    if(!isdigit(*sarg)) printf(temp,sarg[0]);
	    else {
	      iarg = strtol(sarg,NULL,0);
	      printf(temp,iarg&0xFF);
	      }
	    }
	  else printf("(�%s?)",temp);
	  sarg = next_arg(argc,argv);
	  if(loop&&(sarg==NULL)) { f = f1; loop = 0; }
	  }
	}
      }
    else putc(*f,stdout);
    f++;
    if(!*f&&loop) f = f0;
    }
  }
