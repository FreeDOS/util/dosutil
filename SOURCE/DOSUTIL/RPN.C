#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <dos.h>

char cmdbuf[130] = "";
char format[32]  = "";
char dostore     = 0;
char dechar      = 0;
char ignore      = 0;
char alnumarg    = 0;
int  retmode     = 0;
long int cmpval  = 0L;
char digits[18]  = "FEDCBA9876543210";
long int stack[8];
int      sp      = -1;
long int tmpvar;

char helptext[] = "\n\
 RPN 1.2 by J�rgen Hoffmann (2010) j_hoff@hrz1.hrz.tu-darmstadt.de\n\n\
 usage:  rpn [ options ] <expression>\n\n\
 valid options are:\n\
     /S[var]  make set command for environment variable [var]\n\
     /E[var]  store [var] in environment (default: RPN_RESULT)\n\
     /F<str>  output format, <str> must be valid \"C\"\ format\n\
	      in batch files use \"%%%%\" for \"%%\" like /F%%%%4ld\n\
     /D<chr>  tolerate <chr> as decimal separator\n\
     /I<chr>  ignore <chr> inside numbers\n\
     /A[O|X]  alphanumeric argument like e.g. file0033.dat\n\
     /R?[num] return code 1 if result compared to [num] is\n\
       ?:   + greater      += greater or equal    = equal\n\
	    - less than    -= less    or equal    # not equal\n\
	      e.g. /R+5   if [num] is not given, 0 is assumed\n\n\
 expression is in reverse polish notation (RPN) e.g. 12 3 + 5 *\n\
 numbers can be 23 +5 -97 (dec)  0377 (oct)  or  0XF9 (hex)\n\
 operators are  + add   - sub   * mul   / div    \\ mod   ^ xor\n\
		& and   ! or    { shl   } shr   ++ inc\n\
		: dup   $ swap  ~ neg   _ inv   -- dec\n";

void error(int code) {
  if((code==4)&&(sp<0)) code = 2;
  fprintf(stderr,"ERROR: ");
  switch(code) {
    case 1:  fprintf(stderr,"no \"=\" allowed in /%c switch\n",
					      dostore?'E':'S'); break;
    case 2:  fprintf(stderr,"stack is empty\n");                break;
    case 3:  fprintf(stderr,"stack is full\n");                 break;
    case 4:  fprintf(stderr,"only one value on stack\n");       break;
    case 5:  fprintf(stderr,"division by 0\n");                 break;
    case 6:  fprintf(stderr,"no expression\n");                 break;
    case 7:  fprintf(stderr,"out of environment space\n");      break;
    case 8:  fprintf(stderr,"\"=\" in /F conflicts with /%c\n",
					      dostore?'E':'S'); break;
    }
  exit(retmode?0:2);
  }

void put_parentenv(char *cmdp) {
  unsigned int far *ip1; // ptr --> parent's PSP
  unsigned int far *ip2; // ptr --> ptp -->  parent's environment
  unsigned int far *ip3; // ptr --> size  of parent's environment
  int pesize, peused;    // available, actually used
  char far *pebeg;       // ptr --> Parent's Environment BEGin
  char far *peend;       // ptr --> Parent's Environment END
  char far *cp1;
  char far *cp2;
  char far *vpos;        // ptr --> variable, if already present
  int  vlen,vsiz;        // new len, present len, if already present
  vpos = NULL;
  vsiz = 0;
  vlen = (strchr(cmdp,'=') - cmdp) + 1;
  ip1 = MK_FP(_psp,0X0016);
  ip2 = MK_FP(*ip1,0X002C);
  pebeg = MK_FP(*ip2,0);
  ip3 = MK_FP(*ip2-1,3); // !!!!!!!!
  for(cp1=pebeg; *cp1; cp1=MK_FP(FP_SEG(cp1),FP_OFF(cp1)+_fstrlen(cp1)+1))
    if(!_fstrncmp(cp1,cmdp,vlen)) { vpos = cp1; vsiz = _fstrlen(cp1) + 1; }
  peend  = cp1;
  peused = cp1 - pebeg + 1;
  pesize = *ip3 <<  4;
  if(vsiz) {             // if already present, remove it first
    cp1 = vpos;
    cp2 = MK_FP(FP_SEG(cp1),FP_OFF(cp1)+vsiz);
    while(cp2 < peend) *cp1++ = *cp2++;
    peend  = cp1;
    peused -= vsiz;
    }
  if((pesize-peused-5)<strlen(cmdp)) error(7);
  else {                 // now append new copy, if space is sufficient
    _fstrcpy(peend,cmdp);
    cp1  = MK_FP(FP_SEG(peend),FP_OFF(peend)+strlen(cmdp)+1);
    *cp1 = '\0';
    }
  }

char *enter_number(char *p) {
  char tmp[32], *q, *d;
  q = tmp;
  d = &digits[6];
  if((*p=='+')||(*p=='-')) *q++ = *p++;
  if((*p=='0')&&(toupper(p[1])=='X')) {
    *q++ = *p++;
    *q++ = *p++;
    d = digits;
    }
  for( ; *p&&strchr(d,toupper(*p)); p++)
    if((*p!=ignore)&&(q<&tmp[31])) *q++ = *p;
  if(dechar&&(*p==dechar))
    for(p++ ; *p&&strchr(d,*p); p++);
  if(sp>6) error(3);
  else {
    *q = '\0';
    sp++;
    stack[sp] = strtol(tmp,NULL,0);
    }
  p--;
  return(p);
  }

char *resolve_env(char *p) {
  char *e, *q;
  q = &p[strlen(p)-1];
  if((*p!='%')||(*q!='%')) return(p);
  *q = '\0';
  e = getenv(strupr(&p[1]));
  if(e==NULL) {
    *q = '%';
    return(p);
    }
  return(e);
  }

char *eval_alphanumeric(char *p) {
  char tmp[32], fc, *p0,*p1, *p2, *pn, *q, *d;
  int numlen;
  numlen = 0;
  d  = &digits[6];
  p0 = p;
  q  = tmp;
  *q = '0';
  fc = 'u';
  if((alnumarg==16)||(alnumarg==8)) {
    q++;
    fc = 'o';
    d  = &digits[8];
    if(alnumarg==16) {
      *q++ = 'X';
      fc = 'X';
      d  = digits;
      }
    }
  while(*p&&!isdigit(*p)) p++;
  p1 = p;
  for( ; *p&&strchr(d,*p); p++) numlen++;
  p2 = p;
  if(!numlen) p = p0;
  else {
    while(*p) p++;
    p--;
    pn = p1;
    if(alnumarg==10) while((pn<p2)&&(*pn=='0')) pn++;
    while((pn<p2)&&(q<&tmp[31])) *q++ = *pn++;
    *q = '\0';
    enter_number(tmp);
    alnumarg = 0;
    if(!format[0]) {
      q  = format;
      pn = p0;
      while((q<&format[24])&&(pn<p1)) *q++ = *pn++;
      sprintf(q,"%%0%dl%c",numlen,fc);
      q = &format[strlen(format)];
      pn = p2;
      while(*pn&&(q<&format[31])) *q++ = *pn++;
      *q = '\0';
      }
    }
  return(p);
  }

void main (int argc, char* argv[]) {
  int   i,j;
  char *p, *q;

  if(argc < 2) printf(helptext);
  else {
    for(i=1; i < argc && (((*argv[i]=='/')||(*argv[i]=='-'))&&(isalpha(argv[i][1])));i++) {
      switch (toupper(argv[i][1])) {
	case 'A': if(toupper(argv[i][2])=='O') alnumarg = 8;
		  else if(toupper(argv[i][2])=='X') alnumarg = 16;
		  else alnumarg = 10;               break;
	case 'D': dechar  = argv[i][2];             break;
	case 'F': strncpy(format,&argv[i][2],28);   break;
	case 'H': printf(helptext);                 exit(0);
	case 'I': ignore  = argv[i][2];             break;
	case 'E': dostore = 1;
	case 'S': if(argv[i][2]) strncpy(cmdbuf,&argv[i][2],64);
		  if(!cmdbuf[0]) strcpy(cmdbuf,"RPN_RESULT"); break;
	case 'R': switch(argv[i][2]) {
		    case '=': retmode = 1; break;
		    case '+': retmode = 2; break;
		    case '-': retmode = 4; break;
		    case '#': retmode = 6; break;
		    }
		  p = &argv[i][3];
		  if(argv[i][3]=='=') { retmode |= 1; p++; }
		  if(isdigit(*p)) cmpval = strtol(p,NULL,0);
					   break;
	}
      }
    digits[16] = ignore;
    digits[17] = '\0';
    cmdbuf[64] = '\0';
    if(cmdbuf[0]) {
      if(strchr(cmdbuf,'=')) error(1);
      else {
	strupr(cmdbuf);
	strcat(cmdbuf,"=");
	}
      }
    format[31] = '\0';
    if(cmdbuf[0]) if(strchr(format,'=')) error(8);
    if(i >= argc) error(6);
    while(i < argc) {
      for(p=resolve_env(argv[i]); *p; p++) {
	if(isdigit(*p))
	  p = enter_number(p);
	else switch(*p) {
	  case '+': if(p[1]=='+') {
		      p++;
		      if(sp<0) error(2);
		      else stack[sp]++;
		      }
		    else if(isdigit(p[1])) p = enter_number(p);
		    else {
		      if(sp<1) error(4);
		      else {
			sp--;
			stack[sp] += stack[sp+1];
			}
		      }                             break;
	  case '-': if(p[1]=='-') {
		      p++;
		      if(sp<0) error(2);
		      else stack[sp]--;
		      }
		    else if(isdigit(p[1])) p = enter_number(p);
		    else {
		      if(sp<1) error(4);
		      else {
			sp--;
			stack[sp] -= stack[sp+1];
			}
		      }                             break;
	  case '*':if(sp<1) error(4);
		   else {
		     sp--;
		     stack[sp] *= stack[sp+1];
		     }                              break;
	  case '/':if(sp<1) error(4);
		   else {
		     sp--;
		     if(stack[sp+1]==0L) error(5);
		     stack[sp] /= stack[sp+1];
		     }                              break;
	  case '\\':if(sp<1) error(4);
		   else {
		     sp--;
		     if(stack[sp+1]==0L) error(5);
		     stack[sp] %= stack[sp+1];
		     }                              break;
	  case '&':if(sp<1) error(4);
		   else {
		     sp--;
		     stack[sp] &= stack[sp+1];
		     }                              break;
	  case '!':if(sp<1) error(4);
		   else {
		     sp--;
		     stack[sp] |= stack[sp+1];
		     }                              break;
	  case '^':if(sp<1) error(4);
		   else {
		     sp--;
		     stack[sp] ^= stack[sp+1];
		     }                              break;
	  case '{':if(sp<1) error(4);
		   else {
		     sp--;
		     stack[sp] <<= stack[sp+1];
		     }                              break;
	  case '}':if(sp<1) error(4);
		   else {
		     sp--;
		     stack[sp] >>= stack[sp+1];
		     }                              break;
	  case '$':if(sp<1) error(4);
		   else {
		     tmpvar      = stack[sp-1];
		     stack[sp-1] = stack[sp];
		     stack[sp]   = tmpvar;
		     }                              break;
	  case ':':if(sp>6) error(3);
		   else {
		     sp++;
		     stack[sp] = stack[sp-1];
		     }                              break;
	  case '~':if(sp<0) error(2);
		   else {
		     stack[sp] = -stack[sp];
		     }                              break;
	  case '_':if(sp<0) error(2);
		   else {
		     stack[sp] = ~stack[sp];
		     }                              break;
	  default :if(alnumarg&&isalpha(*p)) p = eval_alphanumeric(p);
	  }
	}
      i++;
      }
    if(!strchr(format,'%')) strcat(format,"%ld");
    if(cmdbuf[0]) {
      sprintf(&cmdbuf[strlen(cmdbuf)],format,stack[sp]);
      if(!dostore) printf("SET %s\n",cmdbuf);
      else put_parentenv(cmdbuf);
      }
    else {
      printf(format,stack[sp]);
      printf("\n");
      }
    switch(retmode) {
      case 1: exit(stack[sp]==cmpval);
      case 2: exit(stack[sp]>cmpval);
      case 3: exit(stack[sp]>=cmpval);
      case 4: exit(stack[sp]<cmpval);
      case 5: exit(stack[sp]<=cmpval);
      case 6:
      case 7: exit(stack[sp]!=cmpval);
      }
    }
  exit(0);
  }
